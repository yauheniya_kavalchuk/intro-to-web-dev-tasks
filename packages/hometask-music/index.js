import "./styles/style.scss";
import { renderStatic } from "./components/container/container";
import { renderContent, reloadHeader } from "./contentRender/renderContent";
import { createMainPage } from "./contentRender/mainPage";

const root = document.getElementById('app');
renderStatic(root);

const container = document.querySelector(".container__main");
const defaultPage = createMainPage();
container.append(defaultPage);

root.addEventListener('click', renderContent);
root.addEventListener('click', reloadHeader);