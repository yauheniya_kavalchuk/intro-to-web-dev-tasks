import "./header.scss";
import "../menu/menu.scss";
import { createElement } from "../../utils/createElement";
import { headings } from "./headings";

function createHeadings() {
  const headingsHTML = headings.map((heading) => {
    const listItem = createElement("li", {
      attributes: { class: "header__item header__item--inactive" },
    });
    const link = createElement("a", {
      attributes: { href: heading.href },
      content: heading.title,
    });
    listItem.appendChild(link);

    return listItem;
  });

  return headingsHTML;
}

function createLogo() {
  const logo = createElement("li", {});
  const logoLink = createElement("a", {
    content: "Simo",
    attributes: { class: "header__logo", href: "/" },
  });
  logo.append(logoLink);

  return logo;
}

function createNavigation() {
  const nav = createElement("nav", {});
  const ul = createElement("ul", { attributes: { class: "header__nav menu" } });

  const logo = createLogo();
  ul.appendChild(logo);

  const headingsHTML = createHeadings();
  ul.append(...headingsHTML);

  nav.appendChild(ul);
  return nav;
}

function createHeader() {
  const header = createElement("header", { attributes: { class: "header" } });
  const nav = createNavigation();
  header.appendChild(nav);
  return header;
}

export { createHeader };
