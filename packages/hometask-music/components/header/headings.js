const headings = [
  {
    title: "Discover",
    href: "/discover",
  },
  {
    title: "Join",
    href: "/sign-up",
  },
  {
    title: "Sign In",
    href: "/sign-in",
  },
];

export { headings };
