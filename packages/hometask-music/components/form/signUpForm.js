import "./form.scss";
import { createElement } from "../../utils/createElement";

function createSignUpForm() {
  const form = createElement("form", {
    attributes: { action: "", class: "sign-up__form form" },
  });

  const formLabels = [
    { labelText: "Name:", inputType: "text", placeholder: "Enter your name here" },
    { labelText: "Password:", inputType: "password", placeholder: "" },
    { labelText: "e-mail:", inputType: "email", placeholder: "smth@example.com" },
  ];

  formLabels.forEach((label) => {
    const formLabel = createElement("label", {
      attributes: { class: "form__label" },
    });
    formLabel.textContent = label.labelText;

    const formInput = createElement("input", {
      attributes: { type: label.inputType, class: "form__input", placeholder: label.placeholder },
    });

    formLabel.appendChild(formInput);
    form.appendChild(formLabel);
  });

  return form;
}

export { createSignUpForm };
