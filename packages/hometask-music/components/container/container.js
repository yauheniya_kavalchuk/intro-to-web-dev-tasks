import "./container.scss";
import "../menu/menu.scss";
import { footerLinks } from "./footerLinks";
import { createHeader } from "../header/header";
import { createElement } from "../../utils/createElement";

function createFooter() {
  const footer = createElement("footer", {});
  const list = createElement("ul", { attributes: { class: "menu" } });
  const footerLinksHTML = footerLinks.map((link) =>
    createElement("li", { content: link.title })
  );
  list.append(...footerLinksHTML);
  footer.append(list);
  return footer;
}

function createContainer() {
  const container = createElement("div", {
    attributes: { class: "container" },
  });
  const header = createHeader();
  const main = createElement("div", {
    attributes: { class: "container__main" },
  });
  const footer = createFooter();

 container.append(header, main, footer);
  return container;
}

function renderStatic(parent) {
  const container = createContainer();
  parent.append(container);


  const containerLoaded = new CustomEvent('myCustomEvent', {
    detail: { message: 'Container loaded' },
  });

  container.dispatchEvent(containerLoaded);

  return container;
}

export { renderStatic };
