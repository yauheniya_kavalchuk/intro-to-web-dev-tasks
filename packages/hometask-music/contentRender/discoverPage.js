import "../components/content/content.scss";
import "../components/button/button.scss";
import "../components/content-with-image/content-with-image.scss";
import imageLink from "../assets/images/music-titles.png";
import { createElement } from "../utils/createElement";

function createContentWithImageDiv() {
  const contentWithImageDiv = createElement("div", {
    attributes: { class: "content-with-image" },
  });

  return contentWithImageDiv;
}

function createContentDiv() {
  const contentDiv = createElement("div", {
    attributes: { class: "content content-with-image__content" },
  });

  return contentDiv;
}

function createTitle() {
  const title = createElement("h1", {
    attributes: { class: "content__title" },
    content: "Discover new music",
  });

  return title;
}

function createButtonsDiv() {
  const buttonsDiv = createElement("div", {
    attributes: { class: "content__buttons" },
  });

  const buttons = [{ text: "Charts" }, { text: "Songs" }, { text: "Artists" }];
  buttons.forEach((button) => {
    const buttonElement = createElement("button", {
      attributes: { class: "button" },
      content: button.text,
    });
    buttonsDiv.appendChild(buttonElement);
  });

  return buttonsDiv;
}

function createMainText() {
  const mainText = createElement("p", {
    attributes: { class: "content__main-text" },
    content:
      "By joining, you can benefit by listening to the latest albums released",
  });

  return mainText;
}

function createImageDiv() {
  const imageDiv = createElement("div", {
    attributes: { class: "content-with-image__image" },
  });

  return imageDiv;
}

function createImage() {
  const image = createElement("img", {
    attributes: {
      src: imageLink,
      alt: "music tiles",
    },
  });

  return image;
}

function createDiscoverPage() {
  const contentWithImageDiv = createContentWithImageDiv();
  const contentDiv = createContentDiv();
  const title = createTitle();
  const buttonsDiv = createButtonsDiv();
  const mainText = createMainText();
  const imageDiv = createImageDiv();
  const image = createImage();

  contentDiv.append(title, buttonsDiv, mainText);

  imageDiv.appendChild(image);

  contentWithImageDiv.append(contentDiv, imageDiv);

  return contentWithImageDiv;
}

export { createDiscoverPage };
