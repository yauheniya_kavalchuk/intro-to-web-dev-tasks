import "../components/bg-image/bg-image.scss";
import { createElement } from "../utils/createElement";
import bgImageLink from "/images/background-page-landing.png";

function createContentDiv() {
  const contentDiv = createElement("div", {
    attributes: { class: "content vertical-wrap" },
  });

  return contentDiv;
}

function createTitle() {
  const title = createElement("h1", {
    attributes: { class: "content__title" },
    content: "Feel the music",
  });

  return title;
}

function createMainText() {
  const mainText = createElement("p", {
    attributes: { class: "content__main-text" },
    content: "Stream over 10 million songs with one click",
  });

  return mainText;
}

function createButton() {
  const button = createElement("a", {
    attributes: { class: "button", href: "/sign-up" },
    content: "Join now",
  });

  return button;
}

function createBgImage() {
  const bgImage = createElement("img", {
    attributes: {
      class: "bg-image",
      src: bgImageLink,
      alt: "girl in headphones standing",
    },
  });

  return bgImage;
}

function createMainPage() {
  const contentDiv = createContentDiv();
  const title = createTitle();
  const mainText = createMainText();
  const button = createButton();
  const bgImage = createBgImage();

  contentDiv.append(title, mainText, button, bgImage);

  return contentDiv;
}

export { createMainPage };
