import { routs } from "./routs";

function renderContent(e) {
  e.preventDefault();
  const container = document.querySelector(".container__main");

  if (e.target.tagName !== "A") {
    return;
  }

  const targetLink = e.target.getAttribute("href");

  if (targetLink) {
    loadPage(container, targetLink);
  }
}

function loadPage(container, link) {
  const content = routs[link]();
  container.replaceChildren(content);
}

function reloadHeader(e) {
  const currentLink = document.querySelector('.header__item--active');

  if(currentLink) {
    currentLink.classList.replace("header__item--active", "header__item--inactive");
  }
  const headerItem = e.target.closest('li');
  headerItem.classList.replace("header__item--inactive", "header__item--active");
}

export { renderContent, reloadHeader };
