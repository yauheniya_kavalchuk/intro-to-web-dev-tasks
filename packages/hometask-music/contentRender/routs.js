import { createMainPage } from "./mainPage";
import { createDiscoverPage } from "./discoverPage";
import { createSignUpPage } from "./signUpPage";
import { createSignInPage } from "./signInPage";

const routs = {
  "/": createMainPage,
  "/discover": createDiscoverPage,
  "/sign-up": createSignUpPage,
  "/sign-in": createSignInPage
};

export { routs };
