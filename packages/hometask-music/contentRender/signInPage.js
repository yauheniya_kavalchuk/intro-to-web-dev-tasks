import "../components/content/content.scss";
import "../components/button/button.scss";
import "../components/vertical-wrap/vertical-wrap.scss";
import "../components/form/form.scss";
import imageLink from "/images/background-page-sign-up.png";
import { createElement } from "../utils/createElement";
import { createSignUpForm } from "../components/form/signUpForm";

function createSignInPage() {
  const verticalWrapDiv = createElement("div", {
    attributes: { class: "vertical-wrap" },
  });

  const form = createSignUpForm();
  const button = createButton();
  const bgImage = createElement("img", {
    attributes: {
      class: "bg-image",
      src: imageLink,
      alt: "girl in headphones standing",
    },
  });

  verticalWrapDiv.append(form, button, bgImage);

  return verticalWrapDiv;
}

function createButton() {
  const button = createElement("button", {
    attributes: { class: "sign-up__button button" },
    content: "Sign in",
  });

  return button;
}

export { createSignInPage };
