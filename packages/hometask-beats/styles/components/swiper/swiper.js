// core version + navigation, pagination modules:
import Swiper, { EffectCoverflow, Navigation } from "swiper";
// import Swiper and modules styles
import "swiper/css";
import "swiper/css/navigation";

// init Swiper:
const swiper = new Swiper(".swiper--color", {
  // configure Swiper to use modules
  modules: [Navigation],
  navigation: {
    nextEl: ".swiper__next",
    prevEl: ".swiper__prev",
  },
  breakpoints: {
    768: {
      slidesPerView: 3,
    },
  },
  centerInsufficientSlides: true,
  centeredSlidesBounds: true,
  centeredSlides: true,
});

const swiperProduct = new Swiper(".swiper--product", {
  breakpoints: {
    768: {
      slidesPerView: 3,
    },
  },
  centerInsufficientSlides: true,
  centeredSlidesBounds: true,
  centeredSlides: true,
});